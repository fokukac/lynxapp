<?php

use App\Http\Controllers\Dashboard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redirect;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth:sanctum', 'verified'])->group(function () {

    Route::get('/', function () {
        return view('dashboard');
    });

    Route::get('/logout', function () {
        return view('dashboard');
    });

    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('/users', function () {
        return view('dashboard');
    })->name('users');

    Route::get('/terms', function () {
        return view('dashboard');
    })->name('terms');

    Route::get('accepted-terms', function () {
        return view('terms', [
            'terms' => Auth::user()->acceptedTermToHtml()
        ]);
    })->name('accepted-terms');

    Route::get('accept-terms', function () {
        Auth::user()->acceptNewestTerms();
        return Redirect::back()->with('success', 'Terms accepted.');
    })->name('accept-terms');

});
