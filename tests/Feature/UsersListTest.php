<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Livewire\Livewire;
use App\Http\Livewire\Users;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use phpDocumentor\Reflection\Types\This;

class UsersListTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    public function test_unlogged_user_gets_redirected_to_login_page()
    {
        $this->get('/users')
            ->assertLocation('/login');
    }

    function test_users_listing_page_contains_livewire_component()
    {
        $this->actingAs(User::factory()->create())
            ->get('/users')
            ->assertSeeLivewire('users');
    }

    public function test_logged_in_users_should_land_on_dashboard()
    {
        $this->actingAs(User::factory()->create())
            ->get('/users')
            ->assertViewIs('dashboard');
    }

    public function test_search_user_by_name()
    {
        $name = $this->faker->name . ' ' . ucfirst($this->faker->word());
        User::factory()->create([
            'name' => $name
        ]);

        $this->actingAs(User::factory()->create());

        Livewire::test(Users::class)
            ->set('search', $name)
            ->assertSeeHtml("<span>" . $name . "</span>");
    }
}
