<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Term;
use App\Models\User;
use Livewire\Livewire;
use App\Http\Livewire\Terms;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TermsListTest extends TestCase
{
    use DatabaseTransactions;

    public function test_unlogged_user_gets_redirected_to_login_page()
    {
        $this->get('/terms')
            ->assertLocation('/login');
    }

    function test_terms_listing_page_contains_livewire_component()
    {
        $this->actingAs(User::factory()->create())
            ->get('/terms')
            ->assertSeeLivewire('terms');
    }

    public function test_logged_in_users_should_land_on_dashboard()
    {
        $this->actingAs(User::factory()->create())
            ->get('/terms')
            ->assertViewIs('dashboard');
    }

    public function test_term_can_be_created()
    {
        $this->actingAs(User::factory()->create())->get('/terms');

        Livewire::test(Terms::class)
            ->set('newTerm.name', 'some-title-for-term')
            ->set('newTerm.body', 'some-body-for-term')
            ->call('addTerm');

        $this->assertTrue(Term::where('name', 'some-title-for-term')->exists());
    }
}
