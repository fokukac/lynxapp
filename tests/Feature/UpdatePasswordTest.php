<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Laravel\Jetstream\Http\Livewire\UpdatePasswordForm;
use Livewire\Livewire;
use Tests\TestCase;

class UpdatePasswordTest extends TestCase
{
    use DatabaseTransactions;

    public function test_password_can_be_updated()
    {
        $this->actingAs($user = User::factory()->create());

        Livewire::test(UpdatePasswordForm::class)
                ->set('state', [
                    'current_password' => '123!@#qweQWE',
                    'password' => 'QWE123!@#qwe',
                    'password_confirmation' => 'QWE123!@#qwe',
                ])
                ->call('updatePassword');

        $this->assertTrue(Hash::check('QWE123!@#qwe', $user->fresh()->password));
    }

    public function test_current_password_must_be_correct()
    {
        $this->actingAs($user = User::factory()->create());

        Livewire::test(UpdatePasswordForm::class)
                ->set('state', [
                    'current_password' => 'wrong-password',
                    'password' => 'QWE123!@#qwe',
                    'password_confirmation' => 'QWE123!@#qwe',
                ])
                ->call('updatePassword')
                ->assertHasErrors(['current_password']);

        $this->assertTrue(Hash::check('123!@#qweQWE', $user->fresh()->password));
    }

    public function test_new_passwords_must_match()
    {
        $this->actingAs($user = User::factory()->create());

        Livewire::test(UpdatePasswordForm::class)
                ->set('state', [
                    'current_password' => '123!@#qweQWE',
                    'password' => '123qweQWE!@#',
                    'password_confirmation' => '!@#123qweQWE',
                ])
                ->call('updatePassword')
                ->assertHasErrors(['password']);

        $this->assertTrue(Hash::check('123!@#qweQWE', $user->fresh()->password));
    }
}
