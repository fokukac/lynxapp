<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Terms>
 */
class TermFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->slug(3),
            'body' =>
                '**' . ucwords(implode(' ', $this->faker->words(7))) . '**' . PHP_EOL . PHP_EOL . $this->faker->paragraph(1) .
                PHP_EOL . PHP_EOL . '**' . ucwords(implode(' ', $this->faker->words(4))) . '**' . PHP_EOL . PHP_EOL . $this->faker->paragraph(2) .
                PHP_EOL . PHP_EOL . '**' . ucwords(implode(' ', $this->faker->words(2))) . '**' . PHP_EOL . PHP_EOL . $this->faker->paragraph(3) .
                PHP_EOL . PHP_EOL . '**' . ucwords(implode(' ', $this->faker->words(3))) . '**' . PHP_EOL . PHP_EOL . $this->faker->paragraph(2) .
                PHP_EOL . PHP_EOL . '**' . ucwords(implode(' ', $this->faker->words(2))) . '**' . PHP_EOL . PHP_EOL . $this->faker->paragraph(4),
        ];
    }
}
