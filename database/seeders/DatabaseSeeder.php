<?php

namespace Database\Seeders;

use App\Models\Term;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $five_days_ago = Carbon::now()->subDays(5);
        $four_days_ago = Carbon::now()->subDays(4);
        $three_days_ago = Carbon::now()->subDays(3);
        $before_yesterday = Carbon::now()->subDays(2);
        $yesterday = Carbon::now()->subDays(1);
        $current_time = Carbon::now();


        Term::factory()->create([
            'published_at' => $five_days_ago,
        ]);
        Term::factory()->create([
            'published_at' => $four_days_ago->subHour(),
        ]);
        User::factory(5)->create([
            'terms_accepted_at' => $four_days_ago,
        ]);
        Term::factory(3)->create([
            'published_at' => $three_days_ago->subHour(),
        ]);
        Term::factory(3)->create([
            'published_at' => $three_days_ago,
        ]);
        User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@lynxapp.lop',
            'password' => Hash::make('123!@#qweQWE'),
            'terms_accepted_at' => $before_yesterday,
        ]);
        Term::factory(2)->create([
            'published_at' => $yesterday,
        ]);
        User::factory(3)->create([
            'terms_accepted_at' => $current_time,
        ]);
    }
}
