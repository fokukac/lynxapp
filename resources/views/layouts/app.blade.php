<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    @livewireStyles

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

</head>

<body class="font-sans antialiased">

    @if (Auth::user()->termOutdated())
        <div class="p-5 text-sm text-center text-white bg-red-500">Our <a class="underline" taget="_blank"
                href="{{ config('app.terms_uri') }}">terms of service</a> had been updated. You can view your <a class="underline" target="_blank" href="{{ config('app.accepted_terms_uri') }}">Currently Accepted Terms</a> version of our term and you should <a class="px-3 py-1 mx-1 bg-gray-800 rounded-lg hover:bg-gray-600" href="/accept-terms" >accept</a> the new one.</div>
    @endif
    <x-jet-banner />

    <div class="min-h-screen bg-gray-100">
        @livewire('navigation-menu')


        <!-- Page Heading -->
        @if (isset($header))
            <header class="bg-white shadow">
                <div class="px-4 py-6 mx-auto max-w-7xl sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>
        @endif

        <!-- Page Content -->
        <main class="bg-white">
            {{ $slot }}
        </main>
    </div>

    @stack('modals')

    @livewireScripts


    @if (Session::has('info'))
        <div class="fixed p-2 text-xs text-white bg-blue-600 bottom-4 right-4 rounded-xl">
            {{ Session::get('info') }}
        </div>
    @endif
    @if (Session::has('success'))
        <div class="fixed p-2 text-xs text-white bg-green-600 bottom-4 right-4 rounded-xl">
            {{ Session::get('success') }}
        </div>
    @endif
    @if (Session::has('failure'))
        <div class="fixed p-2 text-xs text-white bg-red-600 bottom-4 right-4 rounded-xl">
            {{ Session::get('failure') }}
        </div>
    @endif

</body>

</html>
