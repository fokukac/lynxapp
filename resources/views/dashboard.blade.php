<x-app-layout>
    <div class="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
        <div class="lg:flex">
            <div class="flex w-full px-4 py-8 bg-blue-100 border-r lg:w-64 lg:overflow-y-auto lg:h-auto lg:flex-col">

                <h2 class="flex items-end mr-10 text-3xl font-semibold text-blue-800 lg:mx-auto">Lynxapp</h2>

                <div class="flex items-end lg:mt-6">
                    <aside>
                        <ul class="flex flex-row items-end lg:items-start lg:flex-col">
                            <li>
                                <a class="flex text-lg font-semibold text-blue-800 lg:px-4 hover:text-blue-900 hover:underline lg:py-2" href="/users">
                                    <span class="flex mx-2 font-medium lg:mx-4">Users</span>
                                </a>
                            </li>
                            <li>
                                <a class="flex text-lg font-semibold text-blue-800 lg:px-4 hover:text-blue-900 hover:underline lg:py-2" href="/terms">
                                    <span class="flex mx-2 font-medium lg:mx-4">Terms</span>
                                </a>
                            </li>

                        </ul>
                    </aside>
                </div>

            </div>

            <div class="flex flex-row p-12 lg:w-full lg:h-auto lg:overflow-y-auto bg-gray-50 lg:flex-col">
                <div class="flex-col items-center justify-center space-y-4">
                    @if (Request::is('users') || Request::is('dashboard'))
                        <livewire:users />
                    @elseif (Request::is('terms'))
                        <livewire:terms />
                    @else
                        <div class="flex items-center justify-center h-screen">Hi {{ Auth::user()->name }}, welcome to the Lynxapp dashboard!</div>
                    @endif
                </div>
            </div>

        </div>
    </div>
</x-app-layout>
