<div class="space-y-5">

    <div x-data="{ show: false }">

        <x-input.button x-on:click="show = ! show"
            class='w-48 px-3 text-2xl font-bold text-center text-white bg-blue-400 border-0 rounded-lg hover:bg-blue-500'>+ Add new</x-input.button>

        <div x-show="show" style="display: none;" class="flex flex-col w-full p-4 my-3 space-y-2 border border-gray-200 rounded-lg bg-gray-50">
            <div>
                <label class="text-xs" for="newname">Name:</label>
                <x-input.text wire:model="newTerm.name" name="newname" id="newname" class="w-full">
                </x-input.text>
            </div>
            <div>
                <label class="text-xs" for="newbody">Body:</label>
                <x-input.textarea rows="20" wire:model="newTerm.body" name="newbody" id="newbody"></x-input.textarea>
            </div>
            <div class="text-right">
                <x-input.button x-on:click="show = false; newbody.value = newname.value = '';" class="text-xs bg-gray-400 hover:bg-gray-500">Cancel</x-input.button>
                <x-input.button x-on:click="show = false; newbody.value = newname.value = '';" wire:click.prevent="addTerm()" class="text-xs bg-green-400 hover:bg-green-500">Save</x-input.button>
            </div>
        </div>
    </div>

    <div class="relative flex justify-center w-full text-gray-400 focus-within:text-gray-600 align-items: center">
        <div class="absolute transform pointer-events-none top-1/4 left-3">&#x1F50E;</div>
        <input
            class="block w-full px-4 py-3 border border-gray-200 appearance-none rounded-xl form-input pl-14 focus:outline-none"
            type='text' wire:model='search' placeholder="Search terms..." />
    </div>

    <x-table>
        <x-slot name="head">
            <x-table.heading sortable wire:click="sortBy('id')" :direction="$sortField === 'id' ? $sortDirection: null">
                Id</x-table.heading>
            <x-table.heading sortable wire:click="sortBy('name')"
                :direction="$sortField === 'name' ? $sortDirection: null">Name</x-table.heading>
            <x-table.heading sortable wire:click="sortBy('body')"
                :direction="$sortField === 'body' ? $sortDirection: null">Body</x-table.heading>
            <x-table.heading sortable wire:click="sortBy('published_at')"
                :direction="$sortField === 'published_at' ? $sortDirection: null">Published
            </x-table.heading>
            <x-table.heading></x-table.heading>
        </x-slot>
        <x-slot name="body">

            @forelse ($terms as $term)
                <x-table.row wire:loading.class.delay="opacity-50" wire:key="{{ $term->id }}">
                    <x-table.cell class="text-center">
                        {{ $term->id }}
                    </x-table.cell>
                    <x-table.cell>
                        @if (isset($term->id) && $term->id === $editedTerm['id'])
                            <input class="text-xs border-gray-200 border-1 rounded-xl" name='name' type='text'
                                wire:model="editedTerm.name" />
                            @error('editedTerm.name')
                                <div class="absolute text-xs text-red-500 left-2">{{ $message }}</div>
                            @enderror
                        @else
                            {{ $term->name }}
                        @endif
                    </x-table.cell>
                    <x-table.cell>
                        @if (isset($term->id) && $term->id === $editedTerm['id'])
                            <textarea class="w-full text-xs border-gray-200 border-1 rounded-xl" rows="30" name='body'
                                wire:model="editedTerm.body"></textarea>
                            @error('editedTerm.body')
                                <div class="absolute text-xs text-red-500 left-2">{{ $message }}</div>
                            @enderror
                        @else
                            {{ $term->body }}
                        @endif
                    </x-table.cell>
                    <x-table.cell class="font-semibold text-center">
                        @if ($term->published_at)
                            <span
                                class="bg-gray-200 inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-{{ $term->published_color }}-100 text-{{ $term->published_color }}-800 capitalize">{{ $term->date_for_humans }}</span>
                        @else
                            <span class="px-2 py-1 bg-gray-200 rounded-full">no</span>
                        @endif
                    </x-table.cell>
                    <x-table.cell>
                        <div class="flex-col space-y-2">
                            @if ($term->published_at === null)
                                <div>
                                    <x-input.button wire:click.prevent="publishTerm({{ $term->id }})"
                                        class='w-full text-center bg-orange-500 hover:bg-orange-400'>
                                        Publish
                                    </x-input.button>
                                </div>
                                <div>
                                    @if ($term->id === $editedTerm['id'])
                                        <x-input.button wire:click.prevent="saveTerm({{ $term->id }})"
                                            class='w-full bg-blue-500 hover:bg-blue-400'>
                                            &#128190; Save
                                        </x-input.button>
                                    @else
                                        <x-input.button wire:click.prevent="editTerm({{ $term->id }})"
                                            class='w-full bg-blue-500 hover:bg-blue-400'>
                                            &#128393; Edit
                                        </x-input.button>
                                    @endif
                                </div>
                                <div>
                                    <x-input.button wire:click="delete({{ $term->id }})" class='w-full bg-red-500 hover:bg-red-400'>
                                        &#10008; Delete</x-input.button>
                                </div>
                            @endif
                        </div>
                    </x-table.cell>
                </x-table.row>
            @empty
                <x-table.row>
                    <x-table.cell colspan='5'>
                        <div class="flex items-center justify-center">
                            <span class="py-8 text-xl font-medium text-cool-gray-400">:( No results for
                                {{ $search }}</span>
                        </div>
                    </x-table.cell>
                </x-table.row>
            @endforelse
        </x-slot>
    </x-table>

    <div>
        {{ $terms->links() }}
    </div>
</div>
