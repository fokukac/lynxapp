<div class="space-y-5">

    <div class="relative flex justify-center w-full text-gray-400 focus-within:text-gray-600 align-items: center">
        <div class="absolute transform pointer-events-none top-1/4 left-3">&#x1F50E;</div>
        <input
            class="block w-full px-4 py-3 border border-gray-200 appearance-none rounded-xl form-input pl-14 focus:outline-none"
            type='text' wire:model='search' placeholder="Search users..." />
    </div>
    <x-table>
        <x-slot name="head">
            <x-table.heading sortable wire:click="sortBy('id')" :direction="$sortField === 'id' ? $sortDirection: null">
                Id</x-table.heading>
            <x-table.heading sortable wire:click="sortBy('name')"
                :direction="$sortField === 'name' ? $sortDirection: null">Name</x-table.heading>
            <x-table.heading sortable wire:click="sortBy('phone')"
                :direction="$sortField === 'phone' ? $sortDirection: null">Phone</x-table.heading>
            <x-table.heading sortable wire:click="sortBy('email')"
                :direction="$sortField === 'email' ? $sortDirection: null">Email</x-table.heading>
            <x-table.heading sortable wire:click="sortBy('email_verified_at')"
                :direction="$sortField === 'email_verified_at' ? $sortDirection: null">Email Confirmed
            </x-table.heading>
            <x-table.heading></x-table.heading>
        </x-slot>
        <x-slot name="body">

            @forelse ($users as $user)
                <x-table.row wire:loading.class.delay="opacity-50" wire:key="{{ $user->id }}"
                    x-on:userDeleted="console.log('deleted' + {{ $user->id }})">
                    <x-table.cell class="text-center">{{ $user->id }}</x-table.cell>
                    <x-table.cell>
                        @if (isset($user->id) && $user->id === $editedUser['id'])
                            <input class="text-xs border-gray-200 border-1 rounded-xl" name='name' type='text'
                                wire:model="editedUser.name" />
                            @error('editedUser.name')
                                <div class="absolute text-xs text-red-500 left-2">{{ $message }}</div>
                            @enderror
                        @else
                            <span>{{ $user->name }}</span>
                        @endif
                    </x-table.cell>
                    <x-table.cell>
                        @if (isset($user->id) && $user->id === $editedUser['id'])
                            <input class="text-xs border-gray-200 border-1 rounded-xl" name='phone' type='tel'
                                wire:model="editedUser.phone" />
                            @error('editedUser.phone')
                                <div class="absolute text-xs text-red-500 left-2">{{ $message }}</div>
                            @enderror
                        @else
                            {{ $user->phone }}
                        @endif
                    </x-table.cell>
                    <x-table.cell>
                        @if (isset($user->id) && $user->id === $editedUser['id'])
                            <input class="text-xs border-gray-200 border-1 rounded-xl" name='email' type='email'
                                wire:model="editedUser.email" />
                            @error('editedUser.email')
                                <div class="absolute text-xs text-red-500 left-2">{{ $message }}</div>
                            @enderror
                        @else
                            {{ $user->email }}
                        @endif
                    </x-table.cell>
                    <x-table.cell>
                        <span
                            class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-{{ $user->email_verified_color }}-100 text-{{ $user->email_verified_color }}-800 capitalize">
                            {{ $user->email_verified_at ? $user->date_for_humans : 'Not Yet Verified' }}
                        </span>
                    </x-table.cell>
                    <x-table.cell>
                        <div class="flex-col space-y-2 text-center">
                            <div>
                                @if ($user->id === $editedUser['id'])
                                    <x-input.button wire:click.prevent="saveUser({{ $user->id }})"
                                        class='w-full bg-blue-500 hover:bg-blue-400'>
                                        &#128190; Save
                                    </x-input.button>
                                @else
                                    <x-input.button wire:click.prevent="editUser({{ $user->id }})"
                                        class='w-full bg-blue-500 hover:bg-blue-400'>
                                        &#128393; Edit
                                    </x-input.button>
                                @endif
                            </div>
                            <div>
                                <x-input.button wire:click="delete({{ $user->id }})"
                                    class='w-full bg-red-500 hover:bg-red-400'>
                                    &#10008; Delete</x-input.button>
                            </div>
                            <div>
                                @if ($user->email_verified_at)
                                    <x-input.button wire:click="unVerify({{ $user->id }})"
                                        class='w-full bg-orange-500 hover:bg-orange-400'>
                                        &#128269; Unverify
                                    </x-input.button>
                                @endif
                            </div>
                        </div>
                    </x-table.cell>
                </x-table.row>
            @empty
                <x-table.row>
                    <x-table.cell colspan='6'>
                        <div class="flex items-center justify-center">
                            <span class="py-8 text-xl font-medium text-cool-gray-400">:( No results for
                                {{ $search }}</span>
                        </div>
                    </x-table.cell>
                </x-table.row>
            @endforelse

        </x-slot>
    </x-table>

    <div>
        {{ $users->links() }}
    </div>
</div>
