<textarea
    {{ $attributes([
        'class' => 'text-xs border-gray-200 border-1 rounded-xl w-full',
        'rows' => '30',
    ]) }}></textarea>
