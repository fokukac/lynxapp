<button type="button" {{ $attributes->merge(['class' => "text-left px-2 py-1 font-bold text-white rounded-full whitespace-nowrap border border-gray-300"]) }}>
    {{ $slot }}
</button>
