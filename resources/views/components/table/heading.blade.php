@props([
    'sortable' => null,
    'direction' => null,
])

<th {{ $attributes->merge(['class' => 'px-6 py-3 bg-gray-100'])->only('class') }}>
    @unless($sortable)
        <span
            class="text-xs font-medium leading-4 tracking-wider text-left uppercase text-cool-gray-500">{{ $slot }}</span>
    @else
        <button {{ $attributes->except('class') }}
            class="flex items-center space-x-1 text-xs font-medium leading-4 text-left">
            <span> {{ $slot }} </span>
            <span>
                <div class="text-sm">
                    @if ($direction === 'asc')
                        △
                    @elseif ($direction === 'desc')
                        ▽
                    @else
                    @endif
                </div>
            </span>
        </button>
    @endunless
</th>
