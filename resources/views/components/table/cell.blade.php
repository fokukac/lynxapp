<td {{ $attributes(['class' => 'px-1 py-2 whitespace-no-wrap text-xs leading-5 text-cool-gray-900']) }}>
    <div class="relative">
        {{ $slot }}
    </div>
</td>
