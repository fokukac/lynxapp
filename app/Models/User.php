<?php

namespace App\Models;

use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Jetstream\HasProfilePhoto;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Date;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'terms_accepted_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function getEmailVerifiedColorAttribute()
    {
        return $this->email_verified_at ? 'green' : 'gray';
    }

    public function getDateForHumansAttribute()
    {
        return Carbon::parse($this->email_verified_at)->format('Y M d h:m:s');
    }

    public function unVerify()
    {
        $this->email_verified_at = null;
        $this->update();
    }

    public function termOutdated()
    {
        return (Term::latestPublishedDate() > $this->terms_accepted_at); // if published term is newer than user's consent
    }

    public function getAcceptedTerm () {
        return Term::whereNotNull('published_at')->where('published_at', '<=', $this->terms_accepted_at)->orderBy('published_at', 'desc')->first();
    }

    public function acceptedTermToHtml()
    {
        $accepted_terms = Term::whereNotNull('published_at')
            ->where('published_at', '<=', $this->terms_accepted_at ?? date('YY mm dd hh:mm:ss', null))->orderBy('published_at', 'desc')->first();
        return Str::markdown($accepted_terms ? $accepted_terms->body : '<div class="font-semibold text-center">No terms accepted yet!</div>');
    }

    public function acceptNewestTerms()
    {
        $this->terms_accepted_at = now();
        $this->save();
    }
}
