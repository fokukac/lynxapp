<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Term extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'body',
        'published_at',
    ];

    public function getPublishedColorAttribute()
    {
        return $this->published_at ? 'green' : 'gray';
    }

    public function getDateForHumansAttribute()
    {
        return Carbon::parse($this->published_at)->format('Y M d h:m:s');
    }

    public static function latestPublishedDate()
    {
        return Term::orderBy('published_at', 'desc')->first()?->published_at ?? null;
    }
}
