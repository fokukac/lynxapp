<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Validation\Rule;
use Laravel\Jetstream\Jetstream;
use Illuminate\Auth\MustVerifyEmail;
use Tests\Feature\EmailVerificationTest;
use App\Actions\Fortify\UpdateUserProfileInformation;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Support\Facades\Redirect;
use Laravel\Fortify\Http\Controllers\EmailVerificationNotificationController;

class Users extends Component
{
    use WithPagination;

    public $search = '';
    public $sortField;
    public $sortDirection = 'asc';
    public $editedUser; // array containing edited user's data

    protected $queryString = ['sortField', 'sortDirection'];

    protected $messages = [
        'editedUser.name.required' => 'User name is required.',
        'editedUser.name.string' => 'User name must be a string.',
        'editedUser.name.max:255' => 'User name can\'t be longer than 255 characters',
        'editedUser.email.required' => 'User email is required.',
        'editedUser.email.email' => 'User email must be a valid email address.',
        'editedUser.email.unique' => 'Somebody has the same email.',
        'editedUser.phone.required' => 'Phone number is required.',
        'editedUser.phone.string' => 'Phone number must be a string.',
        'editedUser.phone.max:255' => 'Phone number can\'t be longer than 255 characters',
    ];

    protected function rules()
    {
        return [
            'editedUser.name' => ['required', 'string', 'max:255'],
            'editedUser.email' => ['required', 'email', Rule::unique('users', 'email')->ignore($this->editedUser['id']), 'max:255'],
            'editedUser.phone' => ['required', 'string', 'max:255'],
        ];
    }



    public function boot()
    {
        $this->resetEdit();
    }

    private function resetEdit()
    {
        $this->editedUser = [
            'id' => null,
            'name' => null,
            'phone' => null,
            'email' => null,
        ];
    }

    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortDirection = $this->sortDirection === 'asc' ? 'desc' : 'asc';
        } else {
            $this->sortDirection = 'asc';
        }
        $this->sortField = $field;
    }

    public function unVerify($userid)
    {
        User::find($userid)->unVerify();
    }

    public function delete($userid)
    {
        $this->emit('userDeleted', $userid);
        User::find($userid)->delete();
        //Redirect::to("/users")->with('success', 'User deleted.');
    }

    public function editUser($userid)
    {
        $user = User::find($userid);
        $this->resetEdit();
        $this->editedUser['id'] = $user->id;
        $this->editedUser['name'] = $user->name;
        $this->editedUser['phone'] = $user->phone;
        $this->editedUser['email'] = $user->email;
    }

    public function saveUser($userid)
    {

        $user = User::find($userid);

        if ($this->editedUser['id'] == $userid) {

            $this->validate();

            (new UpdateUserProfileInformation)->update($user, $this->editedUser);

            $this->resetEdit();

            session()->flash('success', 'Profile successfully updated');

            //Redirect::to("/users")->with('success', 'User info updated.');
        }
    }

    public function render()
    {
        if ($this->sortField === null) {
            $this->sortField = 'id';
        }

        return view('livewire.users', [
            'users' => User::where('name', 'like', '%' . $this->search . '%')
                ->orWhere('email', 'like', '%' . $this->search . '%')
                ->orWhere('phone', 'like', '%' . $this->search . '%')
                ->orderBy($this->sortField, $this->sortDirection)
                ->paginate(5)
        ]);
    }
}
