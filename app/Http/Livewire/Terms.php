<?php

namespace App\Http\Livewire;

use Exception;
use App\Models\Term;
use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Validation\Rule;
use App\Notifications\TermsUpdated;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Notification;

class Terms extends Component
{

    use WithPagination;

    public $search = '';
    public $sortField;
    public $sortDirection = 'desc';
    public $editedTerm; // array containing edited term's data
    public $newTerm; // array containing new term's data

    protected $queryString = ['sortField', 'sortDirection'];

    protected $messages = [
        'editedTerm.name.required' => 'Term name is required.',
        'editedTerm.name.string' => 'Term name must be a string.',
        'editedTerm.name.max:255' => 'Term name can\'t be longer than 255 characters.',
        'editedTerm.name.unique' => 'Name already in use.',
        'editedTerm.name.body' => 'Term body is required.',
        'editedTerm.name.body' => 'Term body must be a string.',
        'editedTerm.body.max:4294967295' => 'Term body can\'t be longer than 4294967295 characters.',
    ];

    protected function rules()
    {
        return [
            'editedTerm.name' => ['required', 'string', 'max:255', Rule::unique('terms', 'name')->ignore($this->editedTerm['id'])],
            'editedTerm.body' => ['required', 'string', 'max:4294967295'],
        ];
    }

    public function boot()
    {
        $this->resetEdit();
        $this->resetNew();
    }

    private function resetEdit()
    {
        $this->editedTerm = [
            'id' => null,
            'name' => null,
            'body' => null,
            'published_at' => null,
        ];
    }

    private function resetNew()
    {
        $this->newTerm = [
            'id' => null,
            'name' => null,
            'body' => null,
            'published_at' => null,
        ];
    }

    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortDirection = $this->sortDirection === 'asc' ? 'desc' : 'asc';
        } else {
            $this->sortDirection = 'asc';
        }
        $this->sortField = $field;
    }

    public function publishTerm($termid)
    {
        $term = Term::find($termid);

        if ($term->published_at !== null) {
            return;
            //Redirect::to("/terms")->with('failure', 'Published terms cannot be republished.');
        }

        $term->published_at = now();
        $term->save();

        Notification::send(User::all(), new TermsUpdated());

        Storage::disk('terms')->put('terms.md', $term->body);

        Redirect::to("/terms")->with('success', 'Term published.');
    }

    public function addTerm()
    {
        $term = new Term;
        $term->name = $this->newTerm['name'];
        $term->body = $this->newTerm['body'];
        $term->save();
        $this->resetNew();
    }

    public function editTerm($termid)
    {
        $term = Term::find($termid);
        $this->resetEdit();
        $this->editedTerm['id'] = $term->id;
        $this->editedTerm['name'] = $term->name;
        $this->editedTerm['body'] = $term->body;
    }

    public function saveTerm($termid)
    {

        $term = Term::find($termid);

        if ($term->published_at !== null) {
            return;
            //Redirect::to("/terms")->with('failure', 'Published terms cannot be edited.');
        }

        if ($this->editedTerm['id'] == $termid) {

            $this->validate();

            $term->name = $this->editedTerm['name'];
            $term->body = $this->editedTerm['body'];
            $term->save();

            $this->resetEdit();

            //Redirect::to("/terms")->with('success', 'Term updated.');
        }
    }

    public function delete($termid)
    {

        $term = Term::find($termid);

        if ($term->published_at !== null) {
            return;
            //Redirect::to("/terms")->with('failure', 'Published terms cannot be deleted.');
        }

        Term::find($termid)->delete();

        //Redirect::to("/users")->with('success', 'User deleted.');
    }

    public function render()
    {
        if ($this->sortField === null) {
            $this->sortField = 'published_at';
        }

        return view('livewire.terms', [
            'terms' => Term::where('name', 'like', '%' . $this->search . '%')
                ->orWhere('body', 'like', '%' . $this->search . '%')
                ->orderBy($this->sortField, $this->sortDirection)
                ->paginate(5)
        ]);
    }
}
