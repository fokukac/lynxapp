<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class UserUnverify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:unverify {userid}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Unverify user based on id';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        User::find($this->argument('userid'))->unVerify();
        echo PHP_EOL . "User's email had beed successfully unverified!" . PHP_EOL;
        return 0;
    }
}
