<?php

namespace App\Console\Commands;

use App\Models\Term;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RemoveUnusedTerms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'terms:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes all published terms that are not the Currently Accepted Terms of any users.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //get all users, who accepted terms
        $users = User::whereNotNull('terms_accepted_at')->orderBy('terms_accepted_at', 'desc')->get();

        //get all term ids that were accepted by somebody
        $accepted_terms = [];
        foreach ($users as $key => $user) {
            if ($user->getAcceptedTerm()) {
                if (isset($accepted_terms[$user->getAcceptedTerm()->id])) {
                    $accepted_terms[$user->getAcceptedTerm()->id]++;
                } else {
                    $accepted_terms[$user->getAcceptedTerm()->id] = 1;
                }
            }
        }
        $accepted_terms = array_flip($accepted_terms);

        //exception:
        //add the newest published term to list of exceptions in case of nobody accepted it yet
        $newest_published_term = Term::whereNotNull('published_at')->orderBy('published_at', 'desc')->first();
        if ($newest_published_term) {
            array_push($accepted_terms, $newest_published_term->id);
        }

        $accepted_terms = array_unique($accepted_terms);

        //get all published terms, that nobody accepted and delete them
        $published_unused_terms = Term::whereNotNull('published_at')->whereNotIn('id', $accepted_terms)->get();
        foreach ($published_unused_terms as $term_to_delete) {
            echo PHP_EOL . "Deleting term: (id: {$term_to_delete->id}), (name: {$term_to_delete->name}), (published on: {$term_to_delete->published_at})" . PHP_EOL;
            $term_to_delete->delete();
        }

        return 0;
    }
}
